const env  =require("dotenv").config({})
const express = require("express")
const app = express()

app.use(express.json({ limit: "50mb" }))
app.get("/", (req, res) => {
    res.send("Hello!!!")
})
app.post("/name", (req, res) => {
    res.send("firstpage")
})
app.get("/api", (req, res) => {
    res.send("apipage")
})

app.disable("x-powered-by")
app.listen(process.env.EXPRESS_PORT, async () => {
    console.log(`App listening on port ${process.env.EXPRESS_PORT}`)
})